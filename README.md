# confluent-kafka-builder-arm64

Extends the Java buildtools image by compiling needed libraries for other Confluent Kafka tools.

Specifically, the following dependencies are built and added to the local Maven repo on the image, par https://github.com/confluentinc/kafka-connect-storage-common/wiki/FAQ
* Confluent Kafka
* Confluent Common
* Conflunet Rest Utils
* Confluent Schema Registry (for Avro converter)
* Kafka Connect Storage Common 
