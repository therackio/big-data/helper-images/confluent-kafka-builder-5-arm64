ARG PARENT_IMAGE
FROM $PARENT_IMAGE

ARG CONFLUENT_VERSION

ENV MAVEN_OPTS="-Xms512m -Xmx7000m -XX:ReservedCodeCacheSize=1g"

RUN \
    set eux && \
    apt update && apt install -y grep git bash curl python3 wget libc6 && \
    git clone https://github.com/confluentinc/kafka/ && \
    cd kafka && \
    git fetch origin refs/tags/v$CONFLUENT_VERSION:refs/tags/v$CONFLUENT_VERSION && \
    git checkout tags/v$CONFLUENT_VERSION && \
    ls -al && \
    export GRADLE_OPTS="-Xmx7g -XX:MaxMetaspaceSize=768m -XX:+HeapDumpOnOutOfMemoryError -Dfile.encoding=UTF-8" && \
    gradle && \
    ./gradlew installAll && \
    cd .. && \
    git clone https://github.com/confluentinc/common/ && \
    cd common && \   
    git fetch origin refs/tags/v$CONFLUENT_VERSION:refs/tags/v$CONFLUENT_VERSION && \
    git checkout tags/v$CONFLUENT_VERSION && \
    ls -al && \
    mvn clean install -DskipTests && \
    cd .. && \
    git clone https://github.com/confluentinc/rest-utils/ && \
    cd rest-utils && \   
    git fetch origin refs/tags/v$CONFLUENT_VERSION:refs/tags/v$CONFLUENT_VERSION && \
    git checkout tags/v$CONFLUENT_VERSION && \
    ls -al && \
    mvn clean install -DskipTests && \
    cd .. && \
    git clone https://github.com/confluentinc/schema-registry/ && \
    cd schema-registry && \   
    git fetch origin refs/tags/v$CONFLUENT_VERSION:refs/tags/v$CONFLUENT_VERSION && \
    git checkout tags/v$CONFLUENT_VERSION && \
    ls -al && \
    mvn clean install -DskipTests && \
    cd .. && \
    git clone https://github.com/confluentinc/kafka-connect-storage-common/ && \
    cd kafka-connect-storage-common && \
    git fetch origin refs/tags/v$CONFLUENT_VERSION:refs/tags/v$CONFLUENT_VERSION && git checkout tags/v$CONFLUENT_VERSION && \
    mvn clean install -DskipTests  && \
    cd .. && \
    pwd
